package com.example.smartdumpster;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.smartdumpster.Utilities.BluetoothUtilities;
import com.example.smartdumpster.Utilities.Extra;
import com.example.smartdumpster.Utilities.InternetUtilities;
import com.example.smartdumpster.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import btLib.exceptions.BluetoothDeviceNotFound;

public class DepositActivity extends AppCompatActivity {

    private TextView timeTextView;
    private String token;
    private int seconds;
    private String username;
    private String name;
    private String dumpster;
    private String dumpsterId;

    Handler handler = new Handler();
    private Runnable runnableTime = new Runnable() {
        @Override
        public void run() {
            seconds = seconds - 1;
            timeTextView.setText(Utility.getStringTime(seconds));
            if(seconds == 0){
                handler.removeCallbacks(runnableTime);
                BluetoothUtilities.getBtChannel().sendMessage("0");
                getTokenLength();
            }
            handler.postDelayed(runnableTime, 1000);
        }
    };

    private Runnable runnableAvalability = new Runnable() {
        @Override
        public void run() {
            handler.removeCallbacks(runnableAvalability);
            getAvailability(dumpsterId);
            handler.postDelayed(runnableAvalability, 5000);
        }
    };

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BluetoothUtilities.ENABLE_BT_REQUEST && resultCode == RESULT_OK) {
            Log.d(BluetoothUtilities.APP_LOG_TAG, "Bluetooth enabled!");
        }

        if (requestCode == BluetoothUtilities.ENABLE_BT_REQUEST && resultCode == RESULT_CANCELED) {
            Log.d(BluetoothUtilities.APP_LOG_TAG, "Bluetooth not enabled!");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.depositConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityPause(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        InternetUtilities.registerNetworkCallback(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(btAdapter != null && !btAdapter.isEnabled()){
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), BluetoothUtilities.ENABLE_BT_REQUEST);
        }

        this.initUI();
    }

    private void initUI(){

        final String trashId = getIntent().getStringExtra(Extra.TRASH_ID.getTypeExtra());
        final String trash = getIntent().getStringExtra(Extra.TRASH.getTypeExtra());
        final String depositId = getIntent().getStringExtra(Extra.DEPOSIT_ID.getTypeExtra());
        final int time = Integer.parseInt(getIntent().getStringExtra(Extra.TIME.getTypeExtra()));

        this.dumpsterId = getIntent().getStringExtra(Extra.DUMPSTER_ID.getTypeExtra());
        this.dumpster = getIntent().getStringExtra(Extra.DUMPSTER.getTypeExtra());
        this.token = getIntent().getStringExtra(Extra.TOKEN_ID.getTypeExtra());
        this.name = getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        this.username = getIntent().getStringExtra(Extra.USERNAME_ID.getTypeExtra());

        final TextView dumpsterTextView = findViewById(R.id.dumpsterTextView);
        final TextView trashTextView = findViewById(R.id.trashTextView);
        this.timeTextView = findViewById(R.id.time_textView);

        dumpsterTextView.setText(dumpster);
        trashTextView.setText(trash);

        this.seconds = time;
        this.timeTextView.setText(Utility.getStringTime(time));

        try {
            final String message = "1" + trashId;
            BluetoothUtilities.connectToBTServer(message);
        } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
            bluetoothDeviceNotFound.printStackTrace();
        }

        findViewById(R.id.extendDeposit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDepositTime(depositId, true);
            }
        });

        findViewById(R.id.endDeposit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDepositTime(depositId, false);
            }
        });

        this.handler.post(runnableTime);
        this.handler.post(runnableAvalability);

        Utility.setUpToolbar(this, this.getString(R.string.app_name));
    }

    private void updateDepositTime(final String depositId, final boolean increment){
        String url ="https://smartdumpster.markz.dev/service/updatedeposittime/" + token +
                "/" + depositId + "/" + increment;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("result").equals("success")){
                                final JSONObject data = response.getJSONObject("data");
                                if(increment){
                                    seconds = Integer.parseInt(data.getString("offset"));
                                }else{
                                    seconds = 1;
                                }
                            } else {
                                Utility.showNeutralAlert(response.getString("message"), DepositActivity.this);
                            }
                        } catch (JSONException e) {
                            Log.e("ERRORE", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    private void getTokenLength(){
        String url ="https://smartdumpster.markz.dev/service/gettokenlength/" + token;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("Response: ", response.toString());
                            if(response.getString("result").equals("success")){
                                final JSONObject data = response.getJSONObject("data");
                                Log.d("Data: ", data.toString());
                                Intent i;
                                if(Integer.parseInt(data.getString("time")) > 0){
                                    i = new Intent(getApplicationContext(), MainActivity.class);
                                    i.putExtra(Extra.USERNAME_ID.getTypeExtra(), username);
                                    i.putExtra(Extra.TOKEN_ID.getTypeExtra(), token);
                                    i.putExtra(Extra.NAME.getTypeExtra(), name);
                                    i.putExtra(Extra.DUMPSTER.getTypeExtra(), dumpster);
                                    i.putExtra(Extra.DUMPSTER_ID.getTypeExtra(), dumpsterId);
                                    i.putExtra(Extra.TIME.getTypeExtra(), data.getString("time"));
                                }else{
                                    i = new Intent(getApplicationContext(), LoginActivity.class);
                                }
                                BluetoothUtilities.getBtChannel().close();
                                startActivity(i);
                            } else {
                                Utility.showNeutralAlert(response.getString("message"), DepositActivity.this);
                            }
                        } catch (JSONException e) {
                            Log.e("ERRORE", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    private void getAvailability(final String dumpsterId){
        String url ="https://smartdumpster.markz.dev/service/getavailability/"+dumpsterId;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("result").equals("success")){
                                final JSONObject data = response.getJSONObject("data");
                                if(data.getString("locked").equals("true")){
                                    final AlertDialog dialog = new AlertDialog.Builder(DepositActivity.this)
                                            .setTitle("Attenzione!")
                                            .setMessage("Il bidone che stai utilizzando è soggetto ad un blocco tecnico, ti invitiamo a riprovare più tardi")
                                            .setCancelable(false)
                                            .setNeutralButton("ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                    getTokenLength();
                                                }
                                            })
                                            .create();
                                    dialog.show();
                                }

                            } else {
                                Utility.showNeutralAlert(response.getString("message"), DepositActivity.this);
                            }
                        } catch (JSONException e) {
                            Log.e("ERRORE", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }
}
