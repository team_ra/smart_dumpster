package com.example.smartdumpster.Utilities;

public enum Extra {

    USERNAME_ID("username_id"),
    TOKEN_ID("token_id"),
    DUMPSTER_ID("dumpster_id"),
    NAME("name"),
    TRASH_ID("trash_id"),
    TRASH("trash"),
    DEPOSIT_ID("deposit_id"),
    TIME("time"),
    DUMPSTER("dumpster");

    private String typeExtra;

    Extra(final String typeExtra){
        this.typeExtra = typeExtra;
    }

    public String getTypeExtra() {
        return this.typeExtra;
    }
}
