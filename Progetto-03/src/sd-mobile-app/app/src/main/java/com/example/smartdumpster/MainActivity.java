package com.example.smartdumpster;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.smartdumpster.Adapters.RadioAdapter;
import com.example.smartdumpster.Adapters.SpinnerAdapter;
import com.example.smartdumpster.Utilities.Extra;
import com.example.smartdumpster.Utilities.InternetUtilities;
import com.example.smartdumpster.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Map<String, String> trashTypes;
    private String username;
    private String name;
    private String dumpsterId;
    private String dumpster;
    private String token;
    private int tokenTime;
    private int seconds;
    private ImageView imageView;

    private Handler handler = new Handler();
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            handler.removeCallbacks(runnableCode);
            getAvailability(dumpsterId, imageView);
            handler.postDelayed(runnableCode, 5000);
        }
    };

    private Runnable runnableCode2 = new Runnable() {
        @Override
        public void run() {
            seconds = seconds - 1;
            final TextView tokenTimeTextView = findViewById(R.id.tokenTimeTextView);
            tokenTimeTextView.setText(Utility.getStringTime(seconds));
            if (seconds == 0){
                handler.removeCallbacks(runnableCode2);

                final Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
            handler.postDelayed(runnableCode2, 1000);
        }
    };

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(MainActivity.class)){
            final Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.putExtra(Extra.USERNAME_ID.getTypeExtra(), this.username);
            i.putExtra(Extra.NAME.getTypeExtra(), this.name);
            i.putExtra(Extra.TOKEN_ID.getTypeExtra(), this.token);
            i.putExtra(Extra.TIME.getTypeExtra(), this.tokenTime);
            i.putExtra(Extra.DUMPSTER.getTypeExtra(), this.dumpster);
            i.putExtra(Extra.DUMPSTER_ID.getTypeExtra(), this.dumpsterId);
            i.putExtra(Extra.TIME.getTypeExtra(), "" + this.seconds);
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        InternetUtilities.registerNetworkCallback(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InternetUtilities.onActivityPause(this);
        InternetUtilities.makeSnackbar(this, R.id.mainConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        initUI();
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityPause(this);
    }

    private void initUI(){
        this.username = getIntent().getStringExtra(Extra.USERNAME_ID.getTypeExtra());
        this.dumpsterId = getIntent().getStringExtra(Extra.DUMPSTER_ID.getTypeExtra());
        this.dumpster = getIntent().getStringExtra(Extra.DUMPSTER.getTypeExtra());
        this.name = getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        this.token = getIntent().getStringExtra(Extra.TOKEN_ID.getTypeExtra());
        this.tokenTime = Integer.parseInt(getIntent().getStringExtra(Extra.TIME.getTypeExtra()));
        this.seconds = this.tokenTime;

        final TextView helloLabel = findViewById(R.id.helloTextView);
        final TextView dumpsterLabel = findViewById(R.id.dumpsterTextView);
        helloLabel.setText(String.format("Ciao, %s!", this.name));
        dumpsterLabel.setText(this.dumpster);

        final ListView listView = this.findViewById(R.id.searchedPlaceListView);
        final RadioAdapter listAdapter = new RadioAdapter(this);
        listView.setAdapter(listAdapter);

        this.trashTypes = new HashMap<>();

        findViewById(R.id.depositButton).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                final String id = String.valueOf(listAdapter.getSelectValue());
                if (InternetUtilities.getIsNetworkConnected()) {
                    //kill del processo quando faccio il deposito
                    handler.removeCallbacks(runnableCode2);
                    startDeposit(id);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        findViewById(R.id.logoutButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout(token);
            }
        });

        this.imageView = this.findViewById(R.id.imageView);
        this.getAvailability(dumpsterId, this.imageView);
        this.getTrashTypes(listAdapter);

        this.handler.post(runnableCode);
        this.handler.post(runnableCode2);

        Utility.setUpToolbar(this, this.getString(R.string.app_name));
    }

    private void getAvailability(final String dumpsterId, final ImageView imageView){
        String url ="https://smartdumpster.markz.dev/service/getavailability/"+dumpsterId;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("result").equals("success")){
                                final JSONObject data = response.getJSONObject("data");
                                final Drawable myDrawable;
                                if(data.getString("available").equals("true")){
                                    myDrawable = getResources().getDrawable(R.drawable.circle_green);
                                }else if (data.getString("locked").equals("true")){
                                    myDrawable = getResources().getDrawable(R.drawable.circle_gold);
                                }else{
                                    myDrawable = getResources().getDrawable(R.drawable.circle_red);
                                }
                                imageView.setImageDrawable(myDrawable);
                            } else {
                                Utility.showNeutralAlert(response.getString("message"), MainActivity.this);
                            }
                        } catch (JSONException e) {
                            Log.e("ERRORE", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    private void startDeposit(final String trashId){
        String url ="https://smartdumpster.markz.dev/service/startdeposit/" + token +  "/" + trashId;
        Log.d("Ciaoo", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("result").equals("success")){
                                final JSONObject data = response.getJSONObject("data");
                                Intent i = new Intent(getApplicationContext(), DepositActivity.class);
                                i.putExtra(Extra.DUMPSTER.getTypeExtra(), dumpster);
                                i.putExtra(Extra.DUMPSTER_ID.getTypeExtra(), dumpsterId);
                                i.putExtra(Extra.TRASH_ID.getTypeExtra(), trashId);
                                i.putExtra(Extra.TRASH.getTypeExtra(), trashTypes.get(trashId));
                                i.putExtra(Extra.DEPOSIT_ID.getTypeExtra(), data.getString("id"));
                                i.putExtra(Extra.TIME.getTypeExtra(), data.getString("time"));
                                i.putExtra(Extra.USERNAME_ID.getTypeExtra(), username);
                                i.putExtra(Extra.NAME.getTypeExtra(), name);
                                i.putExtra(Extra.TOKEN_ID.getTypeExtra(), token);
                                startActivity(i);
                            } else {
                                Utility.showNeutralAlert(response.getString("message"), MainActivity.this);
                            }
                        } catch (JSONException e) {
                            Log.e("ERRORE", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    private void getTrashTypes(final RadioAdapter radioAdapter){
        String url ="https://smartdumpster.markz.dev/service/gettrashtypes";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("result").equals("success")){
                                JSONObject data = response.getJSONObject("data");
                                JSONObject values = (JSONObject) data.get("trash_types");
                                int i = 1;
                                final Map<String, String> trash = new HashMap<>();
                                while (values.has(""+i)){
                                    trash.put(values.getString(""+i), ""+i);
                                    trashTypes.put(""+i, values.getString(""+i));
                                    i++;
                                }
                                radioAdapter.setList(new ArrayList<>(trash.keySet()), trash);
                            } else {
                                Utility.showNeutralAlert(response.getString("message"), MainActivity.this);
                            }
                        } catch (JSONException e) {
                            Log.e("ERRORE", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    private void logout(final String tokenId){
        String url ="https://smartdumpster.markz.dev/service/logout/" + token +  "/" + tokenId;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("result").equals("success")){
                                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(i);
                            } else {
                                Utility.showNeutralAlert(response.getString("message"), MainActivity.this);
                            }
                        } catch (JSONException e) {
                            Log.e("ERRORE", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }
}
