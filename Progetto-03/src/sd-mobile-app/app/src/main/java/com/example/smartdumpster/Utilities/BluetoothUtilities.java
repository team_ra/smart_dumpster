package com.example.smartdumpster.Utilities;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.util.Log;

import java.util.UUID;

import btLib.BluetoothChannel;
import btLib.BluetoothUtils;
import btLib.ConnectToBluetoothServerTask;
import btLib.ConnectionTask;
import btLib.RealBluetoothChannel;
import btLib.exceptions.BluetoothDeviceNotFound;

public class BluetoothUtilities {

    private static final String BT_DEVICE_ACTING_AS_SERVER_NAME = "cgmBT";
    private static final String BT_DEVICE_ACTING_AS_SERVER_PIN ="1234";
    public static final int ENABLE_BT_REQUEST = 1;
    public static final String APP_LOG_TAG = "BT CLN";

    private static BluetoothChannel btChannel;

    public static BluetoothChannel getBtChannel() {
        return btChannel;
    }

    public static void connectToBTServer(final String message) throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(BT_DEVICE_ACTING_AS_SERVER_NAME);

        final UUID uuid = BluetoothUtils.getEmbeddedDeviceDefaultUuid();

        Log.d("UUID", uuid.toString());

        new ConnectToBluetoothServerTask(serverDevice, uuid, new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {
                btChannel = channel;
                channel.sendMessage(message);
                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {
                        Log.d("messageReceive", "RECEIVED FROM "+btChannel.getRemoteDeviceName()+": "+receivedMessage);
                    }

                    @Override
                    public void onMessageSent(String sentMessage) {
                        Log.d("messageSent", "SENT TO "+btChannel.getRemoteDeviceName()+": "+sentMessage);
                    }
                });
            }

            @Override
            public void onConnectionCanceled() {
                Log.d("status", "Unable to connect, device not found!");
            }
        }).execute();
    }
}
