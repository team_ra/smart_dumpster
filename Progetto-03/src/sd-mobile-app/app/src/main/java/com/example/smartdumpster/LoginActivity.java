package com.example.smartdumpster;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.example.smartdumpster.Adapters.SpinnerAdapter;
import com.example.smartdumpster.Utilities.Extra;
import com.example.smartdumpster.Utilities.InternetUtilities;
import com.example.smartdumpster.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private Spinner dumpsterSpinner;
    private Map<String, String> dumpsters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(LoginActivity.class)){
            final Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            this.startActivity(i);
        }else{
            finish();
        }
    }

    private void initUI(){
        final Button loginBtn = findViewById(R.id.login_button);
        this.dumpsterSpinner = findViewById(R.id.spinner);
        this.dumpsters = new HashMap<>();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                final EditText usernameField = findViewById(R.id.username_edit);
                final EditText passwordField = findViewById(R.id.password_edit);

                final String username = usernameField.getText().toString();
                final String password = passwordField.getText().toString();
                final String dumpster = (String)dumpsterSpinner.getSelectedItem();

                if (InternetUtilities.getIsNetworkConnected()) {
                    getToken(username,password,dumpster);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        this.getDumpster();

        Utility.setUpToolbar(this, this.getString(R.string.app_name));
    }

    private void getToken(final String username, final String password, final String dumpster){
        String url ="https://smartdumpster.markz.dev/service/login/"+
                     username + "/"+password+"/"+dumpsters.get(dumpster);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("result").equals("success")){
                                final JSONObject data = response.getJSONObject("data");
                                final Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                i.putExtra(Extra.USERNAME_ID.getTypeExtra(), username);
                                i.putExtra(Extra.TOKEN_ID.getTypeExtra(), data.getString("token"));
                                i.putExtra(Extra.NAME.getTypeExtra(), data.getString("name"));
                                i.putExtra(Extra.DUMPSTER.getTypeExtra(), dumpster);
                                i.putExtra(Extra.DUMPSTER_ID.getTypeExtra(), dumpsters.get(dumpster));
                                i.putExtra(Extra.TIME.getTypeExtra(), data.getString("time"));
                                startActivity(i);
                            } else {
                                showAlert("Attenzione!", response.getString("message"));
                            }
                        } catch (JSONException e) {
                            showAlert("Attenzione!", "Errore di connessione, riprovare");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    private void getDumpster(){
        String url ="https://smartdumpster.markz.dev/service/getdumpsters";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("result").equals("success")){
                                JSONObject data = response.getJSONObject("data");
                                JSONObject values = (JSONObject) data.get("dumpsters");
                                int i = 1;
                                while (values.has(""+i)){
                                    dumpsters.put(values.getString(""+i), ""+i);
                                    i++;
                                }
                                final String[] datas = dumpsters.keySet().toArray(new String[0]);
                                final SpinnerAdapter spinnerAdapter = new SpinnerAdapter(LoginActivity.this, R.id.rowTextView, datas);
                                dumpsterSpinner.setAdapter(spinnerAdapter);
                            } else {
                                showAlert("Attenzione!", response.getString("message"));
                            }
                        } catch (JSONException e) {
                            Log.e("ERRORE", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    private void showAlert(final String title, final String message) {
        final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create();

        dialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.loginConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityPause(this);
    }

}
