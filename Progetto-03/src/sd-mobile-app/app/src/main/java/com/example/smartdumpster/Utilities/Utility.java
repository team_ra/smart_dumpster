package com.example.smartdumpster.Utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.smartdumpster.R;

public class Utility {

    public static void setUpToolbar(AppCompatActivity activity, String title) {
        Toolbar toolbar = activity.findViewById(R.id.app_bar);
        toolbar.setTitle(title);
        activity.setSupportActionBar(toolbar);
    }

    public static void showNeutralAlert(final String message, final Activity activity) {
        final AlertDialog dialog = new AlertDialog.Builder(activity)
                .setTitle("Attenzione!")
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

    public static String getStringTime(final int time){
        final int minutes = Math.round(time/60);
        final int seconds = time - minutes * 60;
        return (minutes<10?"0":"")+ minutes + ":" + (seconds<10?"0":"") + seconds;
    }

}
