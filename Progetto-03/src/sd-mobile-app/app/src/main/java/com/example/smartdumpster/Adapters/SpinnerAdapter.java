package com.example.smartdumpster.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.smartdumpster.R;

public class SpinnerAdapter extends ArrayAdapter<String> {

    private Activity activity;
    private String[] data = null;

    public SpinnerAdapter(final Activity activity, final int resource, final String[] data2) {
        super(activity, R.layout.spinner_layout, resource, data2);
        this.activity = activity;
        this.data = data2;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(R.layout.spinner_layout, parent, false);
        }

        String item = data[position];
        if (item != null) {
            TextView text1 = (TextView) row.findViewById(R.id.rowTextView);
            text1.setText(item);
        }
        return row;
    }
}
