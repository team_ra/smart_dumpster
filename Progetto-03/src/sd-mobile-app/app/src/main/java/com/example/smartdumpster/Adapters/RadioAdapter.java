package com.example.smartdumpster.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.smartdumpster.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class RadioAdapter extends BaseAdapter {

    private final Map<String, String> values;
    private final List<RadioButton> radiogroup;
    private final List<String> list;
    private String selectValue;
    private int selectItemPosition;
    private final Context context;

    public RadioAdapter(final Context context){
        this.list = new ArrayList<>();
        this.radiogroup = new ArrayList<>();
        this.values = new HashMap<>();
        this.context = context;
        this.selectValue = "";
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {
        if(view == null) {
            final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = Objects.requireNonNull(inflater).inflate(R.layout.radio_button_item, viewGroup, false);
        }

        final TextView name = (TextView) view.findViewById(R.id.name);
        final RadioButton radio = (RadioButton) view.findViewById(R.id.radio);
        this.radiogroup.add(i, radio);
        name.setText(this.list.get(i));

        radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectValue.equals(values.get(list.get(i)))){
                    selectItemPosition = -1;
                    selectValue = "";
                    radio.setChecked(false);
                }else{
                    radiogroup.get(selectItemPosition).setChecked(false);
                    selectValue = values.get(list.get(i));
                    radio.setChecked(true);
                    selectItemPosition = i;
                }
                notifyDataSetChanged();
            }
        });

        return view;
    }

    public void setList(final List<String> list, final Map<String, String> values){
        this.list.clear();
        this.list.addAll(list);
        this.values.clear();
        this.values.putAll(values);
        this.notifyDataSetChanged();
    }

    public String getSelectValue() {
        return this.selectValue;
    }
}
