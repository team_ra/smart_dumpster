<?php
define("APIKEY", file_get_contents("key.data"));
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Dashboard</title>
		<meta name="viewport" content="width=device-width">
		<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400&display=swap" rel="stylesheet">
		<style>
			html, body{
				width: 100%;
				height: 100%;
				margin: 0;
				color: #dfe1e6;
				background-color: #15103a;
				font-family: Open Sans;
			}
			button{
				width: 100%;
				font-size: inherit;
				padding: 8px;
				border-radius: 5px;
				border: 2px solid #000;
				background-color: #e3e3e3;
				color: #16103a;
				box-shadow: 0 0 0 1px #FFF;
				margin-top: 10px;
				cursor: pointer;
			}
			button[disabled]{
				opacity: 0.5;
				cursor: not-allowed;
			}
			button:not([disabled]):hover{
				box-shadow: 0 0 0 1px #FFF, inset 0 0 0 2px #000;
			}
			h1{
				margin: 0;
				font-size: 25px;
				text-align: center;
			}
			h1.maintitle{
				text-align: center;
				text-transform: uppercase;
				color: #4CAF50;
				padding-top: 20px;
				font-size: 35px;
				letter-spacing: 2px;
			}
			h2.mainsubtitle{
				text-align: center;
				margin: -10px 0 30px 0;
				font-size: 18px;
				color: #8BC34A;
			}
			div.content{
				margin: auto;
				width: 90%;
				max-width: 1200px;
			}
			div.box{
				display: table;
				width: calc(100% - 20px);
				background-color: #272852;
				border-radius: 3px;
				padding: 15px;
				margin: 10px;
				font-size: 18px;
				text-align: center;
				box-sizing: border-box;
			}
			div.box.hidden{
				display: none;
			}
			div.box[data-section='dumpsterslist'] select{
				width: 100%;
				font-size: inherit;
				outline: none;
			}
			div.box[data-section='stats'], div.box[data-section='data']{
				display: inline-block;
				vertical-align: top;
			}
			div.box table{
				width: 100%;
				text-align: left;
			}
			div.box table td, div.box table th{
				padding: 8px;
			}
			div.box table td.right{
				text-align: right;
			}
			div.box[data-section='stats'] td[data-field='available'][data-available='true']{
				color: #4baf4f;
			}
			div.box[data-section='stats'] td[data-field='available'][data-available='locked']{
				color: #FFC107;
			}
			div.box[data-section='stats'] td[data-field='available'][data-available='false']{
				color: #d80000;
			}
			div.box input[type='date']{
				margin: 3px 0 8px 0;
				font-size: 80%;
			}
			@media screen and (min-width: 1000px) {
				h1.maintitle{
					font-size: 60px;
				}
				h2.mainsubtitle{
					margin-top: -15px;
					letter-spacing: 2px;
					font-size: 24px;
				}
				h1{
					font-size: 32px;
				}
				div.box{
					font-size: 25px;
				}
				div.box[data-section='stats'], div.box[data-section='data']{
					width: calc(50% - 20px);
				}
			}
		</style>
	</head>
	<body>
		<h1 class="maintitle">Smart Dumpster</h1>
		<h2 class="mainsubtitle">G. Amaducci - M. Desiderio - C. Teodorani</h2>
		<div class="content">
			<div class="box" data-section="dumpsterslist">
				Richiesta dei dati in corso...
			</div>
			<div class="box hidden" data-section="stats">
				<h1>Statistiche</h1>
				<table>
					<tr>
						<td>Stato</td>
						<td class="right" data-field="available">...</td>
					</tr>
					<tr>
						<td>Depositi effettuati</td>
						<td class="right" data-field="deposits">...</td>
					</tr>
					<tr>
						<td>Quantit&agrave; corrente</td>
						<td class="right" data-field="amount">...</td>
					</tr>
				</table>
				<button disabled>...</button>
			</div><!--
			--><div class="box hidden" data-section="data">
				<h1>Dati raccolti</h1>
				Seleziona un intervallo di date
				<table>
					<tr>
						<td>Dal</td>
						<td class="right"><input type="date" name="start"></td>
					</tr>
					<tr>
						<td>Al</td>
						<td class="right"><input type="date" name="end"></td>
					</tr>
				</table>
				<button disabled>Carica dati</button>
			</div>
			<div class="box hidden" data-section="datatable"></div>
		</div>
		<script type="text/javascript">
			let selected_dumpster;
			
			document.querySelector("div.box[data-section='stats'] button").addEventListener("click", function(){
				let button = this;
				if(!button.disabled){
					button.disabled = true;
					let dumpster_id = document.querySelector("div.box[data-section='dumpsterslist'] select").value;
					request(service("setlocked", ["<?=APIKEY?>", dumpster_id, this.getAttribute("data-setlocked")]), function(data){
						button.disabled = false;
						updateStats(false);
					});
				}
			});
			document.querySelector("div.box[data-section='data'] button").addEventListener("click", function(){
				let button = this;
				if(!button.disabled){
					button.disabled = true;
					document.querySelector("div.box[data-section='datatable']").innerHTML = "...";
					document.querySelector("div.box[data-section='datatable']").classList.remove("hidden");
					let dumpster_id = document.querySelector("div.box[data-section='dumpsterslist'] select").value;
					let startdate = document.querySelector("div.box[data-section='data'] input[type='date'][name='start']").value;
					let startdate_parts = startdate.split("-");
					let enddate = document.querySelector("div.box[data-section='data'] input[type='date'][name='end']").value;
					let enddate_parts = enddate.split("-");
					if(startdate != "" && enddate != ""){
						request(service("getdata", ["<?=APIKEY?>", dumpster_id, startdate, enddate]), function(data){
							document.querySelector("div.box[data-section='datatable']").innerHTML = "";
							let h1 = document.createElement("h1");
							h1.innerHTML = "Dati dal "+startdate_parts[2]+"-"+startdate_parts[1]+"-"+startdate_parts[0]+" al "+enddate_parts[2]+"-"+enddate_parts[1]+"-"+enddate_parts[0];
							document.querySelector("div.box[data-section='datatable']").appendChild(h1);
							let table = document.createElement("table");
							for(let i=0;i<data["data"]["deposits"].length;i++){
								let tr = document.createElement("tr");
								let td = document.createElement("th");
								td.innerHTML = data["data"]["deposits"][i]["date"]+":";
								tr.appendChild(td);
								td = document.createElement("td");
								td.innerHTML = data["data"]["deposits"][i]["deposits"]==1 ? "Un deposito" : data["data"]["deposits"][i]["deposits"]+" depositi";
								tr.appendChild(td);
								td = document.createElement("td");
								td.innerHTML = "("+data["data"]["deposits"][i]["amount"]+" kg)";
								tr.appendChild(td);
								table.appendChild(tr);
							}
							document.querySelector("div.box[data-section='datatable']").appendChild(table);
							button.disabled = false;
						});
					}else{
						document.querySelector("div.box[data-section='datatable']").innerHTML = "Seleziona un intervallo di date valido";
						button.disabled = false;
					}
				}
			});
			request(service("getdumpsters"), function(data){
				let select = document.createElement("select");
				for(let i in data["data"]["dumpsters"]){
					let option = document.createElement("option");
					option.value = i;
					option.innerHTML = data["data"]["dumpsters"][i];
					select.appendChild(option);
				}
				select.addEventListener("change", function(){
					document.querySelector("div.box[data-section='datatable']").classList.add("hidden");
					updateStats();
				});
				document.querySelector("div.box[data-section='dumpsterslist']").innerHTML = "";
				document.querySelector("div.box[data-section='dumpsterslist']").appendChild(select);
				document.querySelector("div.box[data-section='stats']").classList.remove("hidden");
				document.querySelector("div.box[data-section='data']").classList.remove("hidden");
				updateStats();
				document.querySelector("div.box[data-section='data'] button").disabled = false;
			});
			function updateStats(keepalive=true){
				let dumpster_id = document.querySelector("div.box[data-section='dumpsterslist'] select").value;
				request(service("getstatus", ["<?=APIKEY?>", dumpster_id]), function(data){
					let lockednewstatus = "false";
					if(data["data"]["available"]){
						document.querySelector("div.box[data-section='stats'] td[data-field='available']").innerHTML = "Disponibile";
						document.querySelector("div.box[data-section='stats'] td[data-field='available']").setAttribute("data-available", "true");
						document.querySelector("div.box[data-section='stats'] button").innerHTML = "Rendi non disponibile";
						lockednewstatus = "true";
					}else if(data["data"]["locked"]){
						document.querySelector("div.box[data-section='stats'] td[data-field='available']").innerHTML = "Blocco tecnico";
						document.querySelector("div.box[data-section='stats'] td[data-field='available']").setAttribute("data-available", "locked");
						document.querySelector("div.box[data-section='stats'] button").innerHTML = "Rendi disponibile";
					}else{
						document.querySelector("div.box[data-section='stats'] td[data-field='available']").innerHTML = "In uso";
						document.querySelector("div.box[data-section='stats'] td[data-field='available']").setAttribute("data-available", "false");
						document.querySelector("div.box[data-section='stats'] button").innerHTML = "Rendi disponibile";
					}
					document.querySelector("div.box[data-section='stats'] td[data-field='deposits']").innerHTML = data["data"]["deposits"];
					document.querySelector("div.box[data-section='stats'] td[data-field='amount']").innerHTML = data["data"]["amount"]+" kg";
					document.querySelector("div.box[data-section='stats'] button").setAttribute("data-setlocked", lockednewstatus);
					document.querySelector("div.box[data-section='stats'] button").disabled = false;
					if(keepalive){
						setTimeout(updateStats, 5000);
					}
				});
			}
		
			function service(name, params){
				return "https://smartdumpster.markz.dev/service/"+name+(params != undefined ? "/"+params.join("/") : "");
			}
			function request(url, callback){
				let req = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
				req.open("GET", url, true);
				req.onreadystatechange = function(){
					if(req.readyState == 4 && req.status == 200){
						let data = JSON.parse(req.responseText);
						if(data["result"] == "success"){
							callback(data);
						}else{
							alert("Errore:\n\n"+data["message"]);
						}
					}
				}
				req.send();
			}
		</script>
	</body>
</html>