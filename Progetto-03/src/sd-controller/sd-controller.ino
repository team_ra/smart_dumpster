/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "src/Task.h"
#include "src/led/LedTask.h"
#include "src/servo_motor/MotorTask.h"
#include "src/bluetooth/BluetoothTask.h"

#define LED_A_PIN 4
#define LED_B_PIN 5
#define LED_C_PIN 6
#define MOTOR_PIN 8
#define BL_RXD 10
#define BL_TXD 11
#define TASKS_CYCLE_TIME 10

LedTask ledTask(LED_A_PIN, LED_B_PIN, LED_C_PIN);
MotorTask motorTask(MOTOR_PIN);
BluetoothTask bluetoothTask(BL_RXD, BL_TXD);

Status status = STATUS_CLOSED;
int trashType = -1;

// used to schedule the tasks
unsigned long int startTicks;

void setup()
{
  Serial.begin(9600);
  ledTask.init(&status, &trashType);
  motorTask.init(&status, &trashType);
  bluetoothTask.init(&status, &trashType);
}

// each TAKS_CICLE_TIME ms, all tasks restart
void loop(){
  startTicks = millis();
  ledTask.doStep();
  motorTask.doStep();
  bluetoothTask.doStep();
  int del = startTicks + TASKS_CYCLE_TIME - millis();
  if(del > 0){
    delay(del);
  }
}
