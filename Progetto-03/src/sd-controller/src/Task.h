/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __TASK__
#define __TASK__
#include "Arduino.h"

enum Status
{
	STATUS_OPEN,

	STATUS_CLOSED
};

//This is an abstract class that is used to manage all important shared methods between tasks.
class Task
{

public:
	//init all tasks with a shared variable: Status
	void init(Status *status, int *trashType)
	{
		this->status = status;
		this->trashType = trashType;
	}

	//this method represents the activity that the Task has to do.
	virtual void doStep() = 0;

	//returns the current status of the program.
	Status getStatus()
	{
		return *(this->status);
	}

	//sets the current status with the passed status.
	void setStatus(Status status)
	{
		*(this->status) = status;
	}

	int getTrashType()
	{
		return *(this->trashType);
	}

	void setTrashType(int trashType)
	{
		*(this->trashType) = trashType;
	}

private:
	Status *status;
	int *trashType;
};
#endif
