/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __BLUETOOTH_TASK__
#define __BLUETOOTH_TASK__
#include "../Task.h"
#include "BluetoothImpl.h"

class BluetoothTask : public Task
{

public:
    BluetoothTask(int pinrxd, int pintxd);
    void doStep();

private:
    Bluetooth *bluetooth;
    bool waitForTrashType;
};

#endif
