/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "BluetoothTask.h"
#include "Arduino.h"

BluetoothTask::BluetoothTask(int pinrxd, int pintxd)
{
	this->bluetooth = new BluetoothImpl(pinrxd, pintxd);
	this->waitForTrashType = false;
}

void BluetoothTask::doStep()
{
	char read = this->bluetooth->read();
	if (read != -1 && read != 10)
	{
		if (this->waitForTrashType) {
			// Trash type when operation is '1' (= OPEN)
			this->setTrashType(read - '0');
			this->setStatus(STATUS_OPEN);
			this->waitForTrashType = false;
		} else {
			// Operation to do
			if (read == '1') { // OPEN, wait for trash type
				this->waitForTrashType = true;
			} else { // CLOSED
				this->setStatus(STATUS_CLOSED);
			}
		}
	}
}
