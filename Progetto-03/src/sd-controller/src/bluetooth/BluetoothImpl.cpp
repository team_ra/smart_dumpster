/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "BluetoothImpl.h"
#include "SoftwareSerial.h"
#include "Arduino.h"

BluetoothImpl::BluetoothImpl(int pinrxd, int pintxd)
{
    pinMode(pinrxd, INPUT);
    pinMode(pintxd, OUTPUT);
    this->serial = new SoftwareSerial(pintxd, pinrxd, false);
    this->serial->begin(9600);
}

int BluetoothImpl::read()
{
    return this->serial->read();
}
