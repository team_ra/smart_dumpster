/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __BLUETOOTH_IMPL__
#define __BLUETOOTH_IMPL__
#include "Bluetooth.h"
#include "SoftwareSerial.h"

class BluetoothImpl : public Bluetooth
{

public:
    BluetoothImpl(int pinrxd, int pintxd);
    int read();

private:
    int pinrxd, pintxd;
    SoftwareSerial* serial;
};

#endif
