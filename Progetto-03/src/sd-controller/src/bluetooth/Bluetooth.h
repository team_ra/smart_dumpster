/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __BLUETOOTH__
#define __BLUETOOTH__


class Bluetooth
{

public:
    virtual int read() = 0;

};

#endif
