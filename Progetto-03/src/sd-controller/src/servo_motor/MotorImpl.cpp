/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "MotorImpl.h"
#include "Arduino.h"

MotorImpl::MotorImpl(int pin){
    this->pin = pin;
}

void MotorImpl::on(){
    motor.attach(pin);
}

void MotorImpl::setPosition(int angle){
    motor.write(angle);
}

void MotorImpl::off(){
    motor.detach();
}
