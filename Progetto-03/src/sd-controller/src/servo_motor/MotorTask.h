/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __MOTORTASK__
    #define __MOTORTASK__
    #include "../Task.h"
    #include "MotorImpl.h"
    #define MAX_ANGLE 180
    #define MOTOR_TURN_TIME 1000

    class MotorTask: public Task{
        public:
            MotorTask(int pin);
            void doStep();

        private:
            Motor* motor;
            unsigned long int millisStop;
            bool motorIsOn;
            Status lastStatus;
    };

#endif
