/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __MOTOR__
    #define __MOTOR__

    class Motor{

        public:
            virtual void on() = 0;
            virtual void setPosition(int angle) = 0;
            virtual void off() = 0;
    };

#endif
