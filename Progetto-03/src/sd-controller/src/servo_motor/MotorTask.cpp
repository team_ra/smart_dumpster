/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "MotorTask.h"

MotorTask::MotorTask(int pin) {
	this->motor = new MotorImpl(pin);
	this->lastStatus = STATUS_CLOSED;
	this->millisStop = 0;
}

void MotorTask::doStep() {
	Status thisStatus = this->getStatus();
	if(this->lastStatus != thisStatus){
		this->lastStatus = thisStatus;
		// Lo stato è cambiato se ora il bidone è aperto devo portare il motore a MAX_ANGLE, altrimenti a 0
		if(!this->motorIsOn){
			this->motorIsOn = true;
			this->motor->on();
		}
		this->motor->setPosition(this->lastStatus == STATUS_OPEN ? MAX_ANGLE : 0);
		this->millisStop = millis() + MOTOR_TURN_TIME;
	}
	if(this->millisStop != 0 && this->millisStop < millis()){
		this->motor->off();
		this->motorIsOn = false;
		this->millisStop = 0;
	}
}