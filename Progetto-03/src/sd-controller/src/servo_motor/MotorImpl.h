/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __MOTOR_IMPL__
    #define __MOTOR_IMPL__

    #include "Motor.h"
	#include <Servo.h>

    class MotorImpl: public Motor{

        public:
            MotorImpl(int pin);
            void on();
            void setPosition(int angle);
            void off();

        private:
            int pin;
            Servo motor;
    };

#endif
