/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __LED_TASK__
#define __LED_TASK__
#include "../Task.h"
#include "LedImpl.h"

class LedTask : public Task
{

public:
    LedTask(int pinla, int pinlb, int pinlc);

    void doStep();

private:
    Led *ledA;
    Led *ledB;
    Led *ledC;
};

#endif
