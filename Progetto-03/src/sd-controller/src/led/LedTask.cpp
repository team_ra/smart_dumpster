/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "LedTask.h"
#include "Arduino.h"

LedTask::LedTask(int pinla, int pinlb, int pinlc)
{
	this->ledA = new LedImpl(pinla);
	this->ledB = new LedImpl(pinlb);
	this->ledC = new LedImpl(pinlc);
}

void LedTask::doStep()
{
	if (this->getStatus() == STATUS_CLOSED || this->getTrashType() != 1)
	{
		this->ledA->switchOff();
	}
	else
	{
		this->ledA->switchOn();
	}
	if (this->getStatus() == STATUS_CLOSED || this->getTrashType() != 2)
	{
		this->ledB->switchOff();
	}
	else
	{
		this->ledB->switchOn();
	}
	if (this->getStatus() == STATUS_CLOSED || this->getTrashType() != 3)
	{
		this->ledC->switchOff();
	}
	else
	{
		this->ledC->switchOn();
	}
}
