<?php
error_reporting(E_ALL);
ini_set("display_errors", "On");
$db = mysqli_connect("127.0.0.1:9377", "root", "", "smart_dumpster")or die("<center style='padding-top:50px;font-family:sans-serif;font-size:18px;'>Connessione al database fallita</center>");
mysqli_set_charset($db, "utf8");
date_default_timezone_set('Europe/Rome');

define("TOKEN_DURATION", 4*60); // secondi
define("DEPOSIT_TIME", TOKEN_DURATION/2); // secondi
define("OUTPUT_ERROR_MSG", "Errore inaspettato, riprova.");
define("APIKEY", file_get_contents("key.data"));

$output = array("result" => "error", "message" => OUTPUT_ERROR_MSG);

if(isset($_GET["data"])){
	$params = explode("/", $_SERVER["UNENCODED_URL"]); // $params = array("", "service", "gettoken", "username", "password", "dumpsterid");
	array_shift($params); // remove "";
	array_shift($params); // remove "service";
	$service = array_shift($params); // $service = "gettoken"; $params = array("username", "password", "dumpsterid");
	
	mysqli_query($db, "INSERT INTO api_calls (service, data, datetime) VALUES ('".mysqli_real_escape_string($db, $service)."', '".mysqli_real_escape_string($db, json_encode($params))."', NOW())");
	
	switch(strtolower($service)){
		// Autenticazione
		case "login":
			/*
				Parametri:
				0: username
				1: password
				2: dumpster id
			*/
			if(count($params) < 1 || $params[0] == ""){
				$output["message"] = "Parametro mancante: [1] username";
			}elseif(count($params) < 2 || $params[1] == ""){
				$output["message"] = "Parametro mancante: [2] password";
			}elseif(count($params) < 3 || $params[2] == ""){
				$output["message"] = "Parametro mancante: [3] dumpster id";
			}else{
				$user = mysqli_query($db, "SELECT id,name FROM users WHERE username = '".mysqli_real_escape_string($db, $params[0])."' AND password = '".mysqli_real_escape_string($db, $params[1])."'");
				if(mysqli_num_rows($user) == 0){
					$output["message"] = "Nome utente o password non corretti";
				}else{
					$user = mysqli_fetch_assoc($user);
					if(mysqli_num_rows(mysqli_query($db, "SELECT null FROM dumpsters WHERE id = '".mysqli_real_escape_string($db, $params[2])."'")) == 0){
						$output["message"] = "Bidone non corretto";
					}else{
						$token = mysqli_query($db, "SELECT id, token FROM tokens WHERE user_id = '".mysqli_real_escape_string($db, $user["id"])."' AND dumpster_id = '".mysqli_real_escape_string($db, $params[2])."' AND expiration_date > NOW()");
						if(mysqli_num_rows($token) == 1){
							$token = mysqli_fetch_assoc($token);
							// Esiste un token ancora valido per questo utente, per questo cassonetto: aumento expiration_date
							mysqli_query($db, "UPDATE tokens SET expiration_date = ADDDATE(NOW(), INTERVAL ".TOKEN_DURATION." SECOND) WHERE id = '".mysqli_real_escape_string($db, $token["id"])."'");
							$token = $token["token"];
						}else{
							do{
								$token = generateToken($params[2]);
							}while(mysqli_num_rows(mysqli_query($db, "SELECT null FROM tokens WHERE token = '".mysqli_real_escape_string($db, $token)."' AND expiration_date > NOW()")) > 0);
							mysqli_query($db, "INSERT INTO tokens (user_id, dumpster_id, token, creation_date, expiration_date) VALUES ('".mysqli_real_escape_string($db, $user["id"])."', '".mysqli_real_escape_string($db, $params[2])."', '".mysqli_real_escape_string($db, $token)."', NOW(), ADDDATE(NOW(), INTERVAL ".TOKEN_DURATION." SECOND))");
						}
						$output["result"] = "success";
						$output["data"] = array(
							"name" => $user["name"],
							"token" => $token,
							"time" => TOKEN_DURATION
						);
					}
				}
			}
			break;
		// Informazioni
		case "getdumpsters":
			$dumpsters = mysqli_query($db, "SELECT id,name FROM dumpsters ORDER BY id");
			$output["data"] = array(
				"dumpsters" => array()
			);
			while($dumpster = mysqli_fetch_assoc($dumpsters)){
				$output["data"]["dumpsters"][$dumpster["id"]] = $dumpster["name"];
			}
			$output["result"] = "success";
			break;
		case "gettrashtypes":
			$trash_types = mysqli_query($db, "SELECT id,name FROM trash_types ORDER BY id");
			$output["data"] = array(
				"trash_types" => array()
			);
			while($trash_type = mysqli_fetch_assoc($trash_types)){
				$output["data"]["trash_types"][$trash_type["id"]] = $trash_type["name"];
			}
			$output["result"] = "success";
			break;
		case "getstatus":
			/*
				0: apikey
				1: dumpster id
			*/
			if(count($params) < 1){
				$output["message"] = "Parametro mancante: [1] apikey";
			}elseif($params[0] != APIKEY){
				$output["message"] = "Accesso negato";
			}elseif(count($params) < 2){
				$output["message"] = "Parametro mancante: [2] dumpster id";
			}else{
				$dumpster = mysqli_query($db, "SELECT locked,amount FROM dumpsters WHERE id = '".mysqli_real_escape_string($db, $params[1])."'");
				if(mysqli_num_rows($dumpster) == 0){
					$output["message"] = "Bidone non corretto";
				}else{
					$dumpster = mysqli_fetch_assoc($dumpster);
					$output["result"] = "success";
					$output["data"] = array(
						"available" => $dumpster["locked"] == 0 && mysqli_num_rows(mysqli_query($db, "SELECT null FROM deposits WHERE dumpster_id = '".mysqli_real_escape_string($db, $params[1])."' AND end_date > NOW()")) == 0,
						"locked" => $dumpster["locked"] == 1,
						"deposits" => mysqli_num_rows(mysqli_query($db, "SELECT null FROM deposits WHERE dumpster_id = '".mysqli_real_escape_string($db, $params[1])."'")),
						"amount" => intval($dumpster["amount"])
					);
				}
			}
			break;
		case "getavailability":
			/*
				Parametri:
				0: dumpster id
			*/
			if(count($params) < 1){
				$output["message"] = "Parametro mancante: [1] dumpster id";
			}else{
				$dumpster = mysqli_query($db, "SELECT locked FROM dumpsters WHERE id = '".mysqli_real_escape_string($db, $params[0])."'");
				if(mysqli_num_rows($dumpster) == 0){
					$output["message"] = "Bidone non corretto";
				}else{
					$dumpster = mysqli_fetch_assoc($dumpster);
					$output["result"] = "success";
					$output["data"] = array(
						"available" => $dumpster["locked"] == 0 && mysqli_num_rows(mysqli_query($db, "SELECT null FROM deposits WHERE dumpster_id = '".mysqli_real_escape_string($db, $params[0])."' AND end_date > NOW()")) == 0,
						"locked" => $dumpster["locked"] == 1
					);
				}
			}
			break;
		// Utilizzo dei bidoni
		case "setlocked":
			/*
				0: apikey
				1: dumpster id
				2: locked
			*/
			if(count($params) < 1){
				$output["message"] = "Parametro mancante: [1] apikey";
			}elseif($params[0] != APIKEY){
				$output["message"] = "Accesso negato";
			}elseif(count($params) < 2){
				$output["message"] = "Parametro mancante: [2] dumpster id";
			}else{
				$dumpster = mysqli_query($db, "SELECT locked,amount FROM dumpsters WHERE id = '".mysqli_real_escape_string($db, $params[1])."'");
				if(mysqli_num_rows($dumpster) == 0){
					$output["message"] = "Bidone non corretto";
				}elseif(count($params) < 3){
					$output["message"] = "Parametro mancante: [3] locked";
				}elseif(!in_array($params[2], array("true", "false"))){
					$output["message"] = "Stato locked non corretto";
				}else{
					$dumpster = mysqli_fetch_assoc($dumpster);
					mysqli_query($db, "UPDATE dumpsters SET locked = '".mysqli_real_escape_string($db, $params[2]=="true"?"1":"0")."' WHERE id = '".mysqli_real_escape_string($db, $params[1])."'");
					// se c'è un deposito in corso, devo terminarlo
					mysqli_query($db, "UPDATE deposits SET end_date = NOW() WHERE dumpster_id = '".mysqli_real_escape_string($db, $params[1])."' AND end_date > NOW()");
					$output["result"] = "success";
				}
			}
			break;
		case "getdata":
			/*
				0: apikey
				1: dumpster id
				2: start date
				3: end date
			*/
			if(count($params) < 1){
				$output["message"] = "Parametro mancante: [1] apikey";
			}elseif($params[0] != APIKEY){
				$output["message"] = "Accesso negato";
			}elseif(count($params) < 2){
				$output["message"] = "Parametro mancante: [2] dumpster id";
			}else{
				$dumpster = mysqli_query($db, "SELECT null FROM dumpsters WHERE id = '".mysqli_real_escape_string($db, $params[1])."'");
				if(mysqli_num_rows($dumpster) == 0){
					$output["message"] = "Bidone non corretto";
				}elseif(count($params) < 3){
					$output["message"] = "Parametro mancante: [3] start date";
				}else{
					// Date: yyyy-mm-dd
					$startdate = explode("-", $params[2]);
					if(count($startdate) != 3 || !checkdate($startdate[1], $startdate[2], $startdate[0])){
						$output["message"] = "Data di inizio non valida";
					}elseif(count($params) < 4){
						$output["message"] = "Parametro mancante: [4] end date";
					}else{
						$enddate = explode("-", $params[3]);
						if(count($enddate) != 3 || !checkdate($enddate[1], $enddate[2], $enddate[0])){
							$output["message"] = "Data di fine non valida";
						}elseif($params[2] > $params[3]){
							$output["message"] = "La data di inizio non puo' essere maggiore della data di fine";
						}else{
							$deposits = mysqli_query($db, "SELECT DATE_FORMAT(start_date, '%d-%m-%Y') as date, SUM(amount) as amount, COUNT(*) as deposits FROM deposits WHERE dumpster_id = '".mysqli_real_escape_string($db, $params[1])."' AND start_date BETWEEN '".mysqli_real_escape_string($db, $params[2])."' AND '".mysqli_real_escape_string($db, $params[3])."' GROUP BY date");
							$output["result"] = "success";
							$output["data"] = array(
								"deposits" => array()
							);
							while($deposit = mysqli_fetch_assoc($deposits)){
								$output["data"]["deposits"][] = $deposit;
							}
						}
					}
				}
			}
			break;
		case "gettokenlength":
			/*
				Parametri:
				0: token
			*/
			if(count($params) < 1 || $params[0] == ""){
				$output["message"] = "Parametro mancante: [1] token";
			}else{
				$token = mysqli_query($db, "SELECT expiration_date FROM tokens WHERE token = '".mysqli_real_escape_string($db, $params[0])."'");
				if(mysqli_num_rows($token) == 0){
					$output["message"] = "Token non valido";
				}else{
					$token = mysqli_fetch_assoc($token);
					$output["result"] = "success";
					$output["data"] = array(
						"time" => max(0, strtotime($token["expiration_date"])-time())
					);
				}
			}
			break;
		case "logout":
			/*
				Parametri:
				0: token
			*/
			if(count($params) < 1 || $params[0] == ""){
				$output["message"] = "Parametro mancante: [1] token";
			}else{
				$token = mysqli_query($db, "SELECT user_id,dumpster_id,expiration_date FROM tokens WHERE token = '".mysqli_real_escape_string($db, $params[0])."' AND expiration_date > NOW()");
				if(mysqli_num_rows($token) == 0){
					$output["message"] = "Autenticazione non riuscita";
				}else{
					mysqli_query($db, "UPDATE tokens SET expiration_date = NOW()");
					$output["result"] = "success";
				}
			}
			break;
		case "settrashamount":
			/*
				0: apikey
				1: dumpster id
				2: dumpster amount
			*/
			if(count($params) < 1){
				$output["message"] = "Parametro mancante: [1] apikey";
			}elseif($params[0] != APIKEY){
				$output["message"] = "Accesso negato";
			}elseif(count($params) < 2){
				$output["message"] = "Parametro mancante: [2] dumpster id";
			}else{
				$dumpster = mysqli_query($db, "SELECT null FROM dumpsters WHERE id = '".mysqli_real_escape_string($db, $params[1])."'");
				if(mysqli_num_rows($dumpster) == 0){
					$output["message"] = "Bidone non corretto";
				}else{
					if(count($params) < 3){
						$output["message"] = "Parametro mancante: [3] dumpster amount";
					}elseif(!is_numeric($params[2])){
						$output["message"] = "Quantità non valida";
					}else{
						$lastdeposit = mysqli_query($db, "SELECT dumpster_amount FROM deposits WHERE dumpster_id = '".mysqli_real_escape_string($db, $params[1])."' AND end_date < NOW() ORDER BY end_date DESC LIMIT 1");
						if(mysqli_num_rows($lastdeposit) == 1){
							$lastdeposit = mysqli_fetch_assoc($lastdeposit);
							$lastamount = $lastdeposit["dumpster_amount"];
						}else{
							$lastamount = 0;
						}
						mysqli_query($db, "UPDATE deposits SET amount = '".mysqli_real_escape_string($db, $params[2]-$lastamount)."', dumpster_amount = '".mysqli_real_escape_string($db, $params[2])."' WHERE dumpster_id = '".mysqli_real_escape_string($db, $params[1])."' AND end_date >= NOW()");
						mysqli_query($db, "UPDATE dumpsters SET amount = '".mysqli_real_escape_string($db, $params[2])."' WHERE id = '".mysqli_real_escape_string($db, $params[1])."'");
						if($params[2] == 100){
							mysqli_query($db, "UPDATE dumpsters SET locked = '1' WHERE id = '".mysqli_real_escape_string($db, $params[1])."'");
						}
						$output["result"] = "success";
					}
				}
			}
			break;
		case "startdeposit":
			/*
				Parametri:
				0: token
				1: trash type id
			*/
			if(count($params) < 1 || $params[0] == ""){
				$output["message"] = "Parametro mancante: [1] token";
			}elseif(count($params) < 2 || $params[1] == ""){
				$output["message"] = "Parametro mancante: [2] trash type id";
			}else{
				$token = mysqli_query($db, "SELECT user_id,dumpster_id,expiration_date FROM tokens WHERE token = '".mysqli_real_escape_string($db, $params[0])."' AND expiration_date > NOW()");
				if(mysqli_num_rows($token) == 0){
					$output["message"] = "Autenticazione non riuscita";
				}else{
					$token = mysqli_fetch_assoc($token);
					$dumpster = mysqli_query($db, "SELECT locked FROM dumpsters WHERE id = '".mysqli_real_escape_string($db, $token["dumpster_id"])."'");
					if(mysqli_num_rows($dumpster) == 0){
						$output["message"] = "Bidone non valido";
					}else{
						$dumpster = mysqli_fetch_assoc($dumpster);
						if($dumpster["locked"] == 1){
							$output["message"] = "Il bidone che stai utilizzando è soggetto ad un blocco tecnico, ti invitiamo a riprovare più tardi";
						}elseif(mysqli_num_rows(mysqli_query($db, "SELECT null FROM deposits WHERE dumpster_id = '".mysqli_real_escape_string($db, $token["dumpster_id"])."' AND end_date > NOW()")) == 1){
							$output["message"] = "Bidone non disponibile";
						}elseif(mysqli_num_rows(mysqli_query($db, "SELECT null FROM trash_types WHERE id = '".mysqli_real_escape_string($db, $params[1])."'")) == 0){
							$output["message"] = "Tipo di rifiuto non corretto";
						}else{
							if(strtotime($token["expiration_date"]) < time()+DEPOSIT_TIME){
								// Se il token risulta in scadenza entro il termine del deposito, ne incremento la durata
								mysqli_query($db, "UPDATE tokens SET expiration_date = ADDDATE(NOW(), INTERVAL ".TOKEN_DURATION." SECOND)");
							}
							mysqli_query($db, "INSERT INTO deposits (user_id, dumpster_id, trash_type_id, start_date, end_date) VALUES ('".mysqli_real_escape_string($db, $token["user_id"])."', '".mysqli_real_escape_string($db, $token["dumpster_id"])."', '".mysqli_real_escape_string($db, $params[1])."', NOW(), ADDDATE(NOW(), INTERVAL ".DEPOSIT_TIME." SECOND))");
							$output["result"] = "success";
							$output["data"] = array(
								"id" => mysqli_insert_id($db),
								"time" => DEPOSIT_TIME
							);
						}
					}
				}
			}
			break;
		case "updatedeposittime":
			/*
				Parametri:
				0: token
				1: deposit id
				2: increment (false: stop - true: increment)
			*/
			if(count($params) < 1 || $params[0] == ""){
				$output["message"] = "Parametro mancante: [1] token";
			}elseif(count($params) < 2 || $params[1] == ""){
				$output["message"] = "Parametro mancante: [2] deposit id";
			}elseif(count($params) < 3 || $params[2] == ""){
				$output["message"] = "Parametro mancante: [3] increment";
			}else{
				$token = mysqli_query($db, "SELECT user_id, expiration_date FROM tokens WHERE token = '".mysqli_real_escape_string($db, $params[0])."' AND expiration_date > NOW()");
				if(mysqli_num_rows($token) == 0){
					$output["message"] = "Autenticazione non riuscita";
				}else{
					$token = mysqli_fetch_assoc($token);
					$deposit = mysqli_query($db, "SELECT * FROM deposits WHERE id = '".mysqli_real_escape_string($db, $params[1])."' AND user_id = '".mysqli_real_escape_string($db, $token["user_id"])."' AND end_date > NOW()");
					if(mysqli_num_rows($deposit) == 0){
						$output["message"] = "Deposito non corretto";
					}else{
						$deposit = mysqli_fetch_assoc($deposit);
						if($params[2] == "true"){
							// increment end_date
							$newenddate = "ADDDATE(NOW(), INTERVAL ".DEPOSIT_TIME." SECOND)";
							$offsettime = DEPOSIT_TIME;
						}else{
							// stop deposit
							$newenddate = "NOW()";
							$offsettime = 0;
						}
						mysqli_query($db, "UPDATE deposits SET end_date = ".mysqli_real_escape_string($db, $newenddate)." WHERE id = '".mysqli_real_escape_string($db, $params[1])."'");
						if($params[2] == "true"){
							$tokentimeleft = time()-strtotime($token["expiration_date"]);
							if($tokentimeleft < DEPOSIT_TIME){
								// se il token scade prima del termine del deposito, aumento la durata del token
								mysqli_query($db, "UPDATE tokens SET expiration_date = ADDDATE(expiration_date, INTERVAL ".mysqli_real_escape_string($db, DEPOSIT_TIME-$tokentimeleft)." SECOND) WHERE token = '".mysqli_real_escape_string($db, $params[0])."'");
							}
						}else{
							/*
								// se la richiesta è per terminare il deposito, rimuovo anche la validità del token
								// errore: quando scelgo un bidone, scelgo anche il tipo di rifiuto. se termino di buttare la carta, magari dopo voglio buttare anche la plastica, quindi terminare la sessione non ha senso
								mysqli_query($db, "UPDATE tokens SET expiration_date = NOW() WHERE token = '".mysqli_real_escape_string($db, $params[0])."'");
							*/
						}
						$output["result"] = "success";
						$output["data"] = array(
							"offset" => $offsettime
						);
					}
				}
			}
			break;
	}
}
if($output["result"] == "success" && $output["message"] == OUTPUT_ERROR_MSG){
	unset($output["message"]);
}
$output["service"] = $service;
$output["params"] = $params;
exit(json_encode($output));

function generateToken($dumpster_id){
	$chars = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
	$token = array();
	for($i=0;$i<20;$i++){
		$token[] = $chars[rand(0, count($chars)-1)];
	}
	return join("", $token).(2500+$dumpster_id-array_sum(array_map("ord", $token)));
}





