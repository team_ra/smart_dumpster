/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
 
#include "src/Task.h"
#include "src/wifi/WifiTask.h"
#include "src/led/LedTask.h"
#include "src/pot/PotTask.h"

#define LED_GREEN D1
#define LED_RED D2
#define POT_PIN A0
#define TASKS_CYCLE_TIME 50

LedTask ledTask(LED_GREEN, LED_RED);
PotTask potTask(POT_PIN);
WifiTask wifiTask;

Status status = STATUS_NOT_CONNECTED;
unsigned long int valueToSend = 4294967295; // max unsigned val

// used to schedule the tasks
unsigned long int startTicks;

// init all tasks used in the program and manage the interrupt of the button
void setup(){
  Serial.begin(115200);
  ledTask.init(&status, &valueToSend);
  potTask.init(&status, &valueToSend);
  wifiTask.init(&status, &valueToSend);
}

// each TAKS_CICLE_TIME ms, all tasks restart
void loop(){
  startTicks = millis();
  ledTask.doStep();
  potTask.doStep();
  wifiTask.doStep();
  int del = startTicks + TASKS_CYCLE_TIME - millis();
  if(del > 0){
    delay(del);
  }
}
