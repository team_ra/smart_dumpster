/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __WIFI_IMPL__
    #define __WIFI_IMPL__
    #include "Wifi.h"

    class WifiImpl: public Wifi {

        public:
            WifiImpl();
            bool isConnected();
            void sendTrashAmount(unsigned long int amount);
            int checkReceivedData();

  };

#endif
