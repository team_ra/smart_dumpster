/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __WIFI__
    #define __WIFI__
    #define SSID_NAME "MarkPhone58"
    #define PASSWORD "123412345"

    class Wifi {

        public:
            virtual bool isConnected() = 0;
            virtual void sendTrashAmount(unsigned long int amount) = 0;
            virtual int checkReceivedData() = 0;
    
    };

#endif
