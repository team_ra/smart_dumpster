/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "Arduino.h"
#include "WifiTask.h"

WifiTask::WifiTask() {
    this->wifi = new WifiImpl();
}

void WifiTask::doStep() {
	if(this->wifi->isConnected()){
		switch(this->wifi->checkReceivedData()){
			case 0: //AVAILABLE
				this->setStatus(STATUS_AVAILABLE);
				break;
			case 1: //LOCKED
				this->setStatus(STATUS_LOCKED);
				break;
			case 2: //NOT_AVAILABLE
				this->setStatus(STATUS_NOT_AVAILABLE);
				break;
			case -2:
				this->setStatus(STATUS_NOT_CONNECTED);
				break;
		}
		unsigned long int valueToSend = this->getValueToSend();
		if(valueToSend != 4294967295){
			this->wifi->sendTrashAmount(valueToSend);
			this->setValueToSend(4294967295);
		}
	}
}