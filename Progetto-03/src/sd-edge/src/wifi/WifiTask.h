/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __WIFI_TASK__
    #define __WIFI_TASK__
    #include "../Task.h"
    #include "WifiImpl.h"

    class WifiTask: public Task{

        public:
            WifiTask();
            void doStep();

        private:
            Wifi* wifi;
    };

#endif
