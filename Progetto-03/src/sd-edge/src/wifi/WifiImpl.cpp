/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "Arduino.h"
#include "WifiImpl.h"
#include "ESP8266WiFi.h"
#include "ESP8266HTTPClient.h"
#include "ArduinoJson.h"

WifiImpl::WifiImpl(){
    WiFi.begin(SSID_NAME, PASSWORD);
}

bool WifiImpl::isConnected(){
    return (WiFi.status() == WL_CONNECTED);
}

void WifiImpl::sendTrashAmount(unsigned long int amount){
    if(this->isConnected()){
        HTTPClient http;
        http.begin("http://smartdumpster.markz.dev/service/settrashamount/hm9c8rexu102eu190ue2/1/"+String(amount));
        http.GET();
        http.end();
    }
}

int WifiImpl::checkReceivedData(){
    if(!this->isConnected()){
        return -2;
    }
    HTTPClient http;
    String payload = "";
    StaticJsonDocument<200> doc1, doc2;
    http.begin("http://smartdumpster.markz.dev/service/getavailability/1");
    if (http.GET() > 0) { // Check the returning code
        payload = http.getString();
    }
    http.end();
    if(payload != ""){
        deserializeJson(doc1, payload);
        if(doc1["result"] == "success"){
            String data = doc1["data"];
            deserializeJson(doc2, data);
            String available = doc2["available"];
            if(available == "true"){
                return 0; //AVAILABLE
            }
            String locked = doc2["locked"];
            if(locked == "true"){
                return 1; //LOCKED
            }
            return 2; //NOT_AVAILABLE
        }
    }
    return -1;
}