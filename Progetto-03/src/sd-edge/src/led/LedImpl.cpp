/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "LedImpl.h"
#include "Arduino.h"

LedImpl::LedImpl(int pin){
    this->pin = pin;
    pinMode(pin, OUTPUT);
}

// switch on the led
void LedImpl::switchOn(){
    digitalWrite(pin, HIGH);
}

// switch off the led
void LedImpl::switchOff(){
    digitalWrite(pin, LOW);
};

// return if the led is switched on
bool LedImpl::isOn(){
    return digitalRead(pin) == HIGH;
};
