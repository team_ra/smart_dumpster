/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "LedTask.h"

LedTask::LedTask(int pinlg, int pinlr) {
	this->ledGreen = new LedImpl(pinlg);
	this->ledRed = new LedImpl(pinlr);
	this->blinkTicksCount = LED_BLINK_TICKS_COUNT;
}

void LedTask::doStep() {
	switch(this->getStatus()){
		case STATUS_NOT_CONNECTED:
			this->blink(this->ledGreen, this->ledRed);
			break;
		case STATUS_AVAILABLE:
			this->ledGreen->switchOn();
			this->ledRed->switchOff();
			break;
		case STATUS_LOCKED:
			this->ledGreen->switchOff();
			this->ledRed->switchOn();
			break;
		case STATUS_NOT_AVAILABLE:
			this->ledGreen->switchOff();
			this->blink(this->ledRed);
			break;
	}
}

void LedTask::blink(Led* led){
	if (this->blinkTicksCount == LED_BLINK_TICKS_COUNT) {
		led->switchOn();
	} else if (this->blinkTicksCount == LED_BLINK_TICKS_COUNT / 2) {
		led->switchOff();
	}
	this->updateBlinkCount();
}

void LedTask::blink(Led* led1, Led* led2){
	if (this->blinkTicksCount == LED_BLINK_TICKS_COUNT) {
		led1->switchOn();
		led2->switchOn();
	} else if (this->blinkTicksCount == LED_BLINK_TICKS_COUNT / 2) {
		led1->switchOff();
		led2->switchOff();
	}
	this->updateBlinkCount();
  }

  void LedTask::updateBlinkCount(){
	this->blinkTicksCount--;
	if (this->blinkTicksCount <= 0) {
		this->blinkTicksCount = LED_BLINK_TICKS_COUNT;
	}
  }
