/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __LED_IMPL__
    #define __LED_IMPL__
    #include "Led.h"

    class LedImpl: public Led {

        public:
            LedImpl(int pin);
            // switch on the led
            void switchOn();
            // switch off the led
            void switchOff();
            // return if the led is switched on
            bool isOn();

        private:
            int pin;

  };

#endif
