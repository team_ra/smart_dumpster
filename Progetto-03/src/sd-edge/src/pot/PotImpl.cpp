/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "PotImpl.h"
#include "Arduino.h"

PotImpl::PotImpl(int pin){
    this->pin = pin;
    pinMode(pin, INPUT);
}

// return the POT position (a value between 0 and 1023)
unsigned int PotImpl::getPosition(){
    return analogRead(this->pin);
}
