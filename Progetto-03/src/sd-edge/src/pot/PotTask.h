/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __POT_TASK__
    #define __POT_TASK__
    #include "../Task.h"
    #include "PotImpl.h"
    #define MAX_POT_VALUE 1024

    class PotTask: public Task{

        public:
            PotTask(int pin);
            void doStep();

        private:
            Pot* pot;
			unsigned long int lastPotPosition;
    };

#endif
