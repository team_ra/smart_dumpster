/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#include "PotTask.h"

PotTask::PotTask(int pin) {
    this->pot = new PotImpl(pin);
    this->lastPotPosition = this->pot->getPosition();
}

void PotTask::doStep() {
    if(this->getStatus() == STATUS_NOT_AVAILABLE || this->getStatus() == STATUS_LOCKED){
        unsigned long int actualPosition = (this->pot->getPosition() * 100 / MAX_POT_VALUE);
        if(actualPosition != this->lastPotPosition){
            this->lastPotPosition = actualPosition;
            this->setValueToSend(this->lastPotPosition);
        }
    }
}