/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 3
 */
#ifndef __TASK__
  #define __TASK__
  	#include "Arduino.h"
	#define LED_BLINK_TICKS_COUNT 6

	enum Status {
		STATUS_NOT_CONNECTED,
		STATUS_AVAILABLE,
		STATUS_LOCKED,
		STATUS_NOT_AVAILABLE
	};

	//This is an abstract class that is used to manage all important shared methods between tasks.
	class Task {

		public:
			//init all tasks with a shared variables: Modality, Status and Parameter
			void init(Status* status, unsigned long int* valueToSend){
				this->status = status;
				this->valueToSend = valueToSend;
			}

			//this method represents the activity that the Task has to do.
			virtual void doStep() = 0;

			//returns the current status of the program.
			Status getStatus(){
				return *(this->status);
			}

			//sets the current status with the passed status.
			void setStatus(Status status){
				*(this->status) = status;
			}

			//put in queue the value of the new pot position, ready to be send through wifi
			void setValueToSend(unsigned long int newPosition){
				*(this->valueToSend) = newPosition;
			}

			//returns the current value to send through wifi
			unsigned long int getValueToSend(){
				return *(this->valueToSend);
			}

	    private:
            Status* status;
			unsigned long int* valueToSend;
	  };
#endif
